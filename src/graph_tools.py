import networkx as nx
import numpy as np
import matplotlib.pyplot as plt 
import itertools
import time

def complete_graph(nodes):
    G = nx.empty_graph(nodes, create_using=nx.DiGraph())
    for node_i, node_j in itertools.combinations(range(nodes),2):
        G.add_edge(node_i,node_j)
    return G

def moralize (DG):
    G = DG.to_undirected()
    for node in DG.nodes_iter():
        parents = DG.predecessors(node)
        for parent_i, parent_j in itertools.combinations(parents,2):
            G.add_edge(parent_i,parent_j)
    return G

def triangulate_graph_ineficient(Graph):
    G = Graph.copy()
    TG = G.copy()
    order_G = G.order()
    while order_G > 2: #When there are only two nodes we cannot add nothing else.
        order_G -= 1
        min_fill = min_fill_measure(G)
        indx = min_fill.argmin()
        nodes = G.nodes()
        neighbors = G.neighbors(nodes[indx])
        G.remove_node(nodes[indx])
        for node_i, node_j in itertools.combinations(neighbors,2):
                if not G.has_edge(node_i, node_j):
                    TG.add_edge(node_i, node_j)
                    G.add_edge(node_i, node_j)
    return TG
         
def triangulate(Graph):
    G = Graph.copy()
    TG = G.copy()
    order_G = G.order()
    min_fill = min_fill_measure(G)
    while order_G > 2:
        order_G -= 1
        nodes = G.nodes()
        indx = min_fill.argmin()
        node = nodes[indx]
        neighbors = G.neighbors(node)
        neighbor_set = set(neighbors)
        G.remove_node(node)

        for node_i,node_j in itertools.combinations(neighbors,2):
            indx_i = nodes.index(node_i)
            neighbor_i_set = set(G.neighbors(node_i))
            min_fill[indx_i] -= len(neighbor_i_set.difference(neighbor_set))
            if not G.has_edge(node_i, node_j):
                indx_j = nodes.index(node_j)
                neighbor_j_set = set(G.neighbors(node_j))
                intersection = neighbor_i_set.intersection(neighbor_j_set)
                min_fill[indx_i] += len(neighbor_i_set.difference(intersection))
                min_fill[indx_j] += len(neighbor_j_set.difference(intersection))
                TG.add_edge(node_i, node_j)
                G.add_edge(node_i, node_j)
                for node_k in intersection:
                    indx_k= nodes.index(node_k)
                    min_fill[indx_k] -= 1
        min_fill = np.delete(min_fill,indx) 
        
    return TG

def min_fill_measure(G):
    min_fill = np.zeros([G.order(),1])
    i = 0
    for node in G.nodes_iter():
        neighbors = G.neighbors(node)
        #print i
        for node_j in range(len(neighbors)):
            for node_i in range(node_j):
                #print '(',neighbors[node_i],',',neighbors[node_j],')'
                if not G.has_edge(neighbors[node_i],neighbors[node_j]):
                    min_fill[i] += 1
                    #print 'measure =', min_fill.T
        i += 1
    
    return min_fill

def juntion_tree(clique_list):
    
    G = nx.Graph()
    G.add_nodes_from(range(len(clique_list)))
    for i,j in itertools.combinations(range(len(clique_list)),2):
        clique_i = clique_list[i]
        clique_j = clique_list[j]
        intersection_size = len(clique_i.intersection(clique_j))
        if not intersection_size == 0:
            G.add_edge(i,j,weight = -intersection_size)
    return nx.minimum_spanning_tree(G)
    
if __name__ == '__main__':
    from bma_gct_inducer import extract_cliques_and_separators
    G = nx.fast_gnp_random_graph(10, 0.09, directed= True)

    (cliq,sep)=extract_cliques_and_separators(G)
    print 'Cliques = ', cliq
    print '\n\n\n\n'
    print 'Separtors = ', sep
#    plt.subplot(311)
#    pos = nx.circular_layout(G)
#    nx.draw_networkx(G,pos=pos,with_labels=True)
#    plt.subplot(312)
#    pos = nx.circular_layout(TG)
#    nx.draw_networkx(TG,pos=pos,with_labels=True)
#    plt.subplot(313)
#    pos = nx.circular_layout(junction_T)
#    nx.draw_networkx(junction_T,pos=pos,with_labels=True)
#
#    plt.show()
    