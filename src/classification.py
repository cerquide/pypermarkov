'''
Created on Jul 13, 2012

@author: cerquide
'''

""" A model for classification """
class model:
    """ Gets a matrix n_instances x n_vars describing each instance.
        Returns a matrix n_instances x n_classes where each entry contains 
        the log prob of that class for that instance. """
    def predict(self,x):
        pass
    
""" A model for classification that has the ability to construct models
    leaving out one of the instances used for learning. """
class loo_model(model):
    """ Function that receives one of the instances used for learning 
        (y contains the class and x the remaining attributes) and returns 
        the model learnt without taking into account that instance. """
    def get_loo_model(self,y,x):
        pass
    
