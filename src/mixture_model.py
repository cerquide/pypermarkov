import numpy as np
import maths
import classification
import distributions
import dataset 

class mixture_model(classification.model): 
    def __init__(self,class_prior_ppd, per_class_ppds):
        self.class_prior_ppd = class_prior_ppd
        self.per_class_ppds = per_class_ppds

    def predict(self,x):
        n_predictions = x.shape[0];
        n_classes = len(self.per_class_ppds)

        # Initialize with the base probabilities
        logClassPrior = self.class_prior_ppd.logpdf(x)
        logPredictions = np.zeros([n_predictions,n_classes])
        logPredictions = logPredictions + logClassPrior
        for class_i in range(n_classes):
            r = self.per_class_ppds[class_i].logpdf(x)
            #%s1 = size(logPredictions(:,class_i))
            logPredictions[:,class_i] = logPredictions[:,class_i] + r
        logPredictions = maths.normalizeInLogDomain(logPredictions)
        return logPredictions

    def __str__(self):
        return "[Mixture_model: class_prior_ppd = " + str(self.class_prior_ppd) + "\n per_class_ppds[0] = " + str(self.per_class_ppds[0]) + "]\n" 

class loo_mixture_component:
    def get_loo_model(self,x):
        pass

class mixture_model_loo(mixture_model): 
    def __init__(self,dstr,class_prior_d, class_prior_ppd, per_class_ds, per_class_ppds):
        mixture_model.__init__(self,class_prior_ppd, per_class_ppds)
        self.class_prior_d = class_prior_d
        self.per_class_ds = per_class_ds
        self.dstr = dstr
        #print class_prior_d

    def get_loo_model(self,y,x):
        ppd = self.class_prior_d.get_loo_distribution(y).get_posterior_predictive_distribution()
        #print ppd

        # Divide the instances to be removed by classes
        inst_by_class = dataset.split_by_classes(self.dstr,x,y)

        n_classes = len(self.per_class_ppds)
        ppds = [None]*n_classes
        for class_i in range(n_classes):
            if inst_by_class[class_i].shape[0] > 0:
                ppds[class_i] = self.per_class_ds[class_i].get_loo_model(inst_by_class[class_i])
            else:
                ppds[class_i] = self.per_class_ppds[class_i]
            #print ppds[class_i]
        return mixture_model(ppd,ppds)

    def __str__(self):
        return "[Mixture_model_loo: class_prior_d = " + str(self.class_prior_d) + "\n per_class_ds" + str(self.per_class_ds[0]) + mixture_model.__str__(self)+ "]\n" 
