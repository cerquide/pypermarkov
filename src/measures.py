from __future__ import division

import numpy as np

def cll(dstr,lp,y):
    cll = 0
    for i in range(lp.shape[0]):
        #print y[i]
        y_index = dstr.class_index[y[i,0]];
        cll = cll + lp[i,y_index];
    return cll

def accuracy(dstr,lp,y):
    #rint dstr
    #print lp
    #print y
    max_i = np.argmax(lp,axis=1);
    #print max_i
    pred_classes = dstr.class_values[max_i];
    #print pred_classes
    n_correct = sum(y[:,0]==pred_classes);
    #print n_correct
    return n_correct/y.shape[0];
