import numpy as np
import dataset

def no_selection(dstr, x_train, y_train,x_test,options):
    return x_train, x_test

def mutual_information_selection(dstr,xtrain,ytrain,xtest,options):
    max_num_vars = options['num_vars']
    if dstr.n_vars <= max_num_vars:
        return xtrain, ytrain;
    
    eliminations = dstr.n_vars - max_num_vars   
    inst_by_class = dataset.split_by_classes(dstr,xtrain,ytrain)
    product = np.zeros((dstr.n_classes, dstr.n_vars))
    
    for class_i in range(dstr.n_classes):
        instances = inst_by_class[class_i]
        class_variance= np.var(instances, 0)
        class_log_variance = np.log(class_variance)
        class_probability = float(np.alen(instances)) / dstr.n_instances

        product[class_i, :] = class_probability*class_log_variance
    log_variance = np.log(np.var(xtrain))
    mutual_information = 0.5*(log_variance - np.sum(product, 0))
    selected_vars = np.arange(0,dstr.n_vars,1)
    for iteration in range(eliminations):
        index = mutual_information.argmin()
        selected_vars = selected_vars[ np.arange(dstr.n_vars-iteration) != index]
        mutual_information = mutual_information[ np.arange(dstr.n_vars-iteration) != index]
    new_xtrain = xtrain[:, selected_vars]
    new_xtest = xtest[:, selected_vars]
    return new_xtrain, new_xtest


method_select = {}
method_select['none'] = no_selection
method_select['mutual_information'] = mutual_information_selection

class feature_selection:
    def __init__(self, options):
        self.options = options
        if not self.has_method():
            self.options['method'] = 'none'
    
    def has_method(self):
        return self.options.has_key('method')
    
    def select_variables(self,dstr, x_train, y_train, x_test):
        return method_select[self.options['method']](dstr, x_train, y_train, x_test, options)

if __name__ == '__main__':
    x = np.random.multivariate_normal([1, 0], [[1, 0], [0, 1]], 10)
    y =np.array([1, 1, 1, 1, 1, 1, 1, 2, 2, 2])
    print "x = "
    print x
    dstr = dataset.dataset_structure(x, y);
    options = {}
    options['num_vars'] = 1
    options['method'] = 'mutual_information'
    f1 = feature_selection(options)
    x_train,  x_test = f1.select_variables(dstr, x, y, x)
    print 'x_train ='
    print x_train
