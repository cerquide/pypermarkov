from __future__ import division
import scipy.linalg as linalg;
import numpy as np;

def quadratic_form_of_inverse(M,x):
    tmp1 = linalg.lstsq(M,x.transpose())[0];
    return (tmp1 * x.transpose()).sum(0);
       
def make_symmetric(M):
    return np.triu(M)+ np.triu(M,1).transpose()

def cov_always_matrix(x):
    #print x.shape
    n_instances, n_vars= x.shape
    if n_instances == 1:
        sigma = np.zeros([x.shape[1],x.shape[1]])
    else:
        sigma = np.cov(x,rowvar=0)
        if n_vars == 1:
            sigma.shape=(1,1)
    return sigma

def normalizeInLogDomain(logpp):
    return logpp - sumInLogDomain(logpp);

def sumInLogDomain(logpp):
    max_p = np.amax(logpp,axis=1);
    max_p.shape = (max_p.size,1);
    logpp = logpp - max_p;
    epp = np.exp(logpp);
    s = np.sum(epp,axis=1);
    s.shape = (s.size,1);
    return max_p+np.log(s);

def mean_remove(n,mu,n_1,mu_1):    
    mu_2 = (n*mu-n_1*mu_1)/(n-n_1)
    return mu_2

def cov_remove(n,mu,c,n_1,mu_1,c_1,n_2,mu_2):
    """ Source for the formulas: 
        "Formulas for Robust, One-Pass Parallel
        Computation of Covariances and
        Arbitrary-Order Statistical Moments" """

    delta_2_1 = mu_2 - mu_1
    delta_2_1.shape = (delta_2_1.shape[0],1)
    d = np.dot(delta_2_1,delta_2_1.T)
    
    return (((n - 1) * c - (n_1 - 1) * c_1 - (n_1 * n_2 / n) * d)) / (n_2 - 1)

if __name__ == '__main__':
    n = 10
    x = np.random.normal(0,1,(n,2))
    mu = np.mean(x,axis=0)
    c = cov_always_matrix(x)
    print "Full covariance is",c
    n_1 = 2
    n_2 = n - n_1
    c_directly = cov_always_matrix(x[0:n_2,:])
    print "Covariance missing the last",n_1," observations (assessed directly) is ",c_directly
    mu_1 = np.mean(x[n_2:n,:],axis=0)
    c_1 = cov_always_matrix(x[n_2:n,:])
    mu_2 = np.mean(x[0:n_2,:],axis=0)
    c_from_previous = cov_remove(n,mu,c,n_1,mu_1,c_1,n_2,mu_2)
    print "Covariance missing the last",n_1," observations (removing from previous) is ",c_from_previous
    assert (linalg.norm(c_directly - c_from_previous)<1e-10)
    
    
    
