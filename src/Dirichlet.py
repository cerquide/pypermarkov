'''
Created on Jul 12, 2012

@author: cerquide
'''

import numpy as np
import distributions
import hypermarkov
import maths
import multinomial
import hypermarkov

class Dirichlet(hypermarkov.conjugate_distribution):
    def __init__(self,class_values,weights):
        self.class_values = class_values;
        self.weights = weights
        
    def incorporate_evidence(self,y):
        for class_i in range(len(self.class_values)):
            self.weights[0,class_i] += np.count_nonzero(y==self.class_values[class_i])
        
    def remove_evidence(self,y):
        for class_i in range(len(self.class_values)):
            self.weights[0,class_i] -= np.count_nonzero(y==self.class_values[class_i])

    def get_posterior_predictive_distribution(self):
        return multinomial.multinomial(maths.normalizeInLogDomain(np.log(self.weights)))
    
    def logpdf(self,x):
        pass
        #ppd = self.get_posterior_predictive_distribution()
        #return ppd.logpdf()

    def __str__(self):
        return "[Dirichlet: class_values ="+self.class_values.__str__() + "\n weights= "+self.weights.__str__()+"]\n"
    
def laplacePrior(dstr):
    return Dirichlet(dstr.class_values,np.ones((1,dstr.n_classes)))

def mlPrior(dstr):
    return Dirichlet(dstr.class_values,np.zeros((1,dstr.n_classes)))
