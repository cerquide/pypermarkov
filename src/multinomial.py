'''
Created on Jul 13, 2012

@author: cerquide
'''
import numpy as np

class multinomial:
    def __init__(self,logps):
        self.logps = logps
        
    def logpdf(self,x = None):
        return self.logps

    def __str__(self):
        return "[Multinomial: logps ="+self.logps.__str__() +"\n ps = "+np.exp(self.logps).__str__()+"]\n"
