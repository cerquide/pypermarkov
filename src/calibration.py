'''
Created on 12/07/2012

@author: victor
'''
import numpy as np
from scipy import optimize

import maths
def get_calibration_method(name):
    if name == 'potential':
        return potential_calibration()
    else:
        return None
    

#
#def cll(alpha, logp_class, logp):
#    sum1 = alpha * logp_class
#    sum2 = maths.sumInLogDomain(alpha*logp)
#    return np.sum(sum2 - sum1)
#
#def cll_prime(alpha,logp_class, logp):
#    p_alpha = np.exp(alpha*logp)
#    sum1 = np.sum(p_alpha*logp,axis=1)
#    sum2 = np.sum(p_alpha,axis=1)
#    division = sum1/sum2
#    return np.sum(division - logp_class
#    )

def cll_and_cllprime(alpha, log_x_ic, log_x_ij):
    alpha = np.max(alpha)
    log_x_ij_times_alpha = alpha * log_x_ij
    #print 'alpha = ', alpha
    log_normalizer_i = maths.sumInLogDomain(log_x_ij_times_alpha)
    log_x_ic_times_alpha = alpha * log_x_ic
    lg_alpha_ic = log_x_ic_times_alpha - log_normalizer_i
    cll = np.array(-np.sum(lg_alpha_ic))
    #cll = np.zeros((1,1))
    #cll[0,0]= -np.sum(lg_alpha_ic)
    #print 'cll=',z.shape
    

    lg_alpha_ij = log_x_ij_times_alpha - log_normalizer_i
    log_log_x_ij = np.log(- log_x_ij)
    normalizer_2 = np.exp(maths.sumInLogDomain(lg_alpha_ij + log_log_x_ij))
    lg_alpha_ic_quote = log_x_ic + normalizer_2
    cllprime = np.array(-np.sum(lg_alpha_ic_quote))
    #print "cll=",cll,"cllprime=",cllprime;
    return (cll,cllprime)


def cll(alpha, log_x_ic, log_x_ij):
    alpha = np.max(alpha)
    log_x_ij_times_alpha = alpha * log_x_ij
    print 'alpha = ', alpha
    log_normalizer_i = maths.sumInLogDomain(log_x_ij_times_alpha)
    log_x_ic_times_alpha = alpha * log_x_ic
    lg_alpha_ic = log_x_ic_times_alpha - log_normalizer_i
    z = np.zeros((1,1))
    z[0,0]= -np.sum(lg_alpha_ic)
    print 'cll=',z.shape
    return z
def cll_prime(alpha,log_x_ic, log_x_ij):       
    alpha = np.max(alpha)
    log_x_ij_times_alpha = alpha * log_x_ij
    log_normalizer_i = maths.sumInLogDomain(log_x_ij_times_alpha)
    lg_alpha_ij = log_x_ij_times_alpha - log_normalizer_i
    log_log_x_ij = np.log(- log_x_ij)
    normalizer_2 = np.exp(maths.sumInLogDomain(lg_alpha_ij + log_log_x_ij))
    lg_alpha_ic_quote = log_x_ic + normalizer_2
    z = np.zeros((1,1))
    z[0,0] = -np.sum(lg_alpha_ic_quote)
    print 'cll_prime=',z.shape
    return z

class calibration_method:
    def __init__(self):
        pass
    def asses_parameters(self):
        pass
    def calibrate(self):
        pass
    
class potential_calibration(calibration_method):
    def __init__(self,logp = None, y = None,dstr=None):
        if logp == None or y == None or dstr == None:
            self.alpha = 1
        else:
            self.alpha = self.asses_parameters(logp,y,dstr)
    
    def asses_parameters(self, logp,dstr,y):
        self.alpha = 0
        log_x_ic = np.zeros((logp.shape[0],1))
        for i in range(logp.shape[0]):
            y_index = dstr.class_index[y[i,0]];
            log_x_ic[i,0] = logp[i,y_index];
#    return cll
#        
#        
#        
#        log_x_ic = np.zeros((logp.shape[0],1))
#        for i in range(logp.shape[0]):
#            for label in y:
#                indx = np.where(dstr.class_values==label)
#                log_x_ic[i,0] = logp[i,indx]
        #result = minimize(cll, 1.,args=(logp_class,logp),method='L-BFGS-B')
        normalizer = maths.sumInLogDomain(logp)
        cosa = log_x_ic-normalizer
        #print "Initial CLL = ",np.sum(cosa)
        result = optimize.fmin_l_bfgs_b(cll_and_cllprime,np.array([1.]),args=(log_x_ic,logp));
        print result
        self.alpha = result[0]
        #print "alpha =",self.alpha

    def calibrate(self,logp):
        return maths.normalizeInLogDomain(self.alpha * logp)
    

class plat_calibration(calibration_method):
    def _init_(self,logp = None):
        if logp == None:
            self.A = 1
            self.B = 0
        else:
            self.A, self.B = self.asses_parameters(logp)
    
    def asses_parameters(self):
        pass
   
    def calibrate(self,logp):
        pass
