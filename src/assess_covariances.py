import dataset;
import numpy as np
import scipy.io;

def assess_cmi(dstr,xtrain,ytrain):
    inst_by_class = dataset.split_by_classes(dstr,xtrain,ytrain);
    
    model_theta = np.zeros(dstr.n_classes);
    for class_i in range(dstr.n_classes):
        model_theta[class_i] = inst_by_class[class_i].shape[0];
    model_theta = model_theta/np.sum(model_theta);

    mi = np.zeros([dstr.n_vars,dstr.n_vars]);
    mi_tmp = np.zeros([dstr.n_vars,dstr.n_vars]);
    for class_i in range(dstr.n_classes):
        print "A"
        mi_tmp = np.corrcoef(inst_by_class[class_i],rowvar=0);
        print mi_tmp
        print "B"
        
        mi_tmp *= -mi_tmp
        print mi_tmp
        print "C"
        mi_tmp += 1
        
        print mi_tmp
        print "D"
        mi_tmp = np.log(mi_tmp)
        
        print mi_tmp
        print "E"
        mi_tmp *= model_theta[class_i]
        
        print mi_tmp
        print "F"
        mi += mi_tmp
        print mi
    mi = -mi
    mi[np.diag_indices(mi.shape[0])]=0;

    print mi
    return mi    

ovarian = dataset.dataset_handle('ovarian-cancer','../data/ovarian_cancer.mat');
d = dataset.load(ovarian.filename)
#x = split_by_classes
z = {} 
#ccfull= np.corrcoef(d.x,rowvar=0)
z['mi'] = assess_cmi(d.str,d.x,d.y)
scipy.io.savemat('tmp2',z);
