from __future__ import division;
import numpy as np;
import scipy.stats as ss;
import scipy.linalg as linalg;
import math;

import maths;
import mvn;

class cond_normal: 
    def __init__(self,mu,beta,sigma_2):
        self.n_condicionants = beta.shape[0];
        self.mu = mu;
        self.beta = beta;
        self.sigma_2 = sigma_2;

    def logcpdf(self,x,y):
        if (self.n_condicionants == 0):
            try:
                logp = ss.norm.logpdf(y,loc=self.mu,scale=math.sqrt(self.sigma_2));
            except:
                logp = ss.norm.logpdf(y,loc=self.mu,scale=0)
        else:
            cbx = np.dot(self.beta,x.transpose());
            pmu = self.mu + cbx;
            try:
                logp = ss.norm.logpdf(y,loc=pmu,scale=math.sqrt(self.sigma_2));
            except:
                logp = ss.norm.logpdf(y,loc=pmu,scale=0)
            #logp2 = mvn.logpdf(y,pmu,self.sigma_2);
            #print logp-logp2;
        return logp;

class cond_normal_loo(cond_normal): 
    def __init__(self,n,mu_z,cov_z,mu,beta,sigma_2):
        self.n = n
        self.mu_z = mu_z
        self.cov_z = cov_z
        cond_normal.__init__(self,mu,beta,sigma_2)

    def get_loo_model(self,y,x):
        #print "x =",x," y=",y
        z = np.hstack((x, y))
        #print "z.shape=",z.shape
        n_1 = z.shape[0]
        muTmp_1 = np.mean(z,axis = 0)
        muTmp = maths.mean_remove(self.n,self.mu_z,n_1,muTmp_1)
        cov_1 = maths.cov_always_matrix(z)
        sigmaTmp = maths.cov_remove(self.n,self.mu_z,self.cov_z,n_1,muTmp_1,cov_1,self.n-n_1,muTmp)      
        mu,beta,sigma_2 = _assess_parameters(muTmp,sigmaTmp,self.n_condicionants)

        return cond_normal_loo(self.n,self.mu_z,self.cov_z,mu,beta,sigma_2)        

def learn_ml(x,y):
    n_instances,n_condicionants = x.shape;
    z = np.hstack((x, y))
    #print "z = ",z
    sigmaTmp = maths.cov_always_matrix(z)
    #print "sigmaTmp =",sigmaTmp
    muTmp = np.mean(z,axis = 0)
    mu,beta,sigma_2 = _assess_parameters(muTmp,sigmaTmp,n_condicionants)
    return cond_normal_loo(n_instances,muTmp,sigmaTmp,mu,beta,sigma_2)

def _assess_parameters(muTmp,sigmaTmp,n_condicionants):
    if (n_condicionants == 0):
        mu = muTmp[-1]
        beta = np.array([])
        sigma_2 = sigmaTmp[-1,-1]
    else: 
        sigmaAA = sigmaTmp[0:n_condicionants,0:n_condicionants]
        sigmaBA = sigmaTmp[[-1],0:n_condicionants]
        beta = np.ravel(linalg.lstsq(sigmaAA,sigmaBA.transpose())[0])
        mu = muTmp[-1] - np.dot(beta,muTmp[0:n_condicionants])
        sigma_2 = sigmaTmp[-1,-1] - np.dot(beta,sigmaBA.T)
            
    return mu,beta,sigma_2
       
  


