"""
Methods for learning structures of Bayesian Classifiers. 
"""

from copy import copy
from sklearn.cross_validation import LeaveOneOut, KFold

import dataset
import graph_tools
import itertools
import math

import networkx as nx
import numpy as np
    
def wcGJAN_learner(dstr, xtrain, ytrain,options,evaluate_network):
    """
    Build wraper condenser Gaussian Joint Augmented Naive Bayes structure (wcGJAN) following:
    Perez, A. (2010). Supervised classification in continuous domains with Bayesian networks. Universidad del Pais Vasco.
    :param dstr: data_structure Object containing information of the data.
    :param xtrain: matrix of predictor variables corresponding to the training instances.
    :param ytrain: vector containing the classes of the training instances.
    :param options: options dictionary.
    :param evaluate_network: A function to evaluate the performance of a structure.
    """
    #obtain fold
    dstr = dataset.dataset_structure(xtrain,ytrain)
    #cv = LeaveOneOut(dstr.n_instances,indices=True)
    cv = LeaveOneOut(dstr.n_instances,indices=True)
    #initialize structure
    best_DG = graph_tools.complete_graph(dstr.n_vars)
    best_accuracy = evaluate_network(dstr, best_DG, xtrain, ytrain, cv)
    #explore while evaluate
    changed = True
    while changed:
        changed = False
        DG = best_DG.copy()
        nodes = DG.nodes()
        for node in nodes:
            new_DG = DG.copy()
            new_DG.remove_node(node)
            new_accuracy = evaluate_network(dstr, new_DG, xtrain, ytrain, cv)
            if new_accuracy > best_accuracy:
                best_accuracy = new_accuracy
                best_DG = new_DG.copy()
                changed = True
    return best_DG
    
def wbGJAN_learner(dstr,xtrain,ytrain,options,evaluate_network):
    """
    Build wraper backwards Gaussian Joint Augmented Naive Bayes structure (wbGJAN) following:
    Perez, A. (2010). Supervised classification in continuous domains with Bayesian networks. Universidad del Pais Vasco.
    :param dstr: data_structure Object containing information of the data.
    :param xtrain: matrix of predictor variables corresponding to the training instances.
    :param ytrain: vector containing the classes of the training instances.
    :param options: options dictionary.
    :param evaluate_network: A function to evaluate the performance of a structure.
    """
    dstr = dataset.dataset_structure(xtrain,ytrain)
    #cv = LeaveOneOut(dstr.n_instances,indices=True)
    cv = LeaveOneOut(dstr.n_instances,indices=True)
    best_DG = nx.DiGraph()
    best_DG.add_nodes_from(range(dstr.n_vars))
    best_accuracy = evaluate_network(dstr,best_DG,xtrain,ytrain,cv)
    changed = True
    while changed:
        changed = False
        DG = best_DG.copy()
        nodes = DG.nodes()
        for node_i, node_j in itertools.combinations(nodes,2):
            new_DG = DG.copy()
            new_DG.remove_node(node_i)
            new_accuracy = evaluate_network(dstr, new_DG, xtrain, ytrain, cv)
            if new_accuracy > best_accuracy:
                best_accuracy = new_accuracy
                best_DG = new_DG.copy()
                changed = True
            new_DG = DG.copy()
            if not new_DG.has_edge(node_i,node_j):
                new_DG.add_edge(node_i,node_j)
                new_accuracy = evaluate_network(dstr, new_DG, xtrain, ytrain, cv)
                if new_accuracy > best_accuracy:
                    best_accuracy = new_accuracy
                    best_DG = new_DG.copy()
                    changed = True
    return best_DG

def wfGJAN_learner(dstr,xtrain,ytrain,options,evaluate_network):
    """
    Build wraper forward Gaussian Joint Augmented Naive Bayes structure (wfGJAN) following:
    Perez, A. (2010). Supervised classification in continuous domains with Bayesian networks. Universidad del Pais Vasco.
    :param dstr: data_structure Object containing information of the data.
    :param xtrain: matrix of predictor variables corresponding to the training instances.
    :param ytrain: vector containing the classes of the training instances.
    :param options: options dictionary.
    :param evaluate_network: A function to evaluate the performance of a structure.
    """
    dstr = dataset.dataset_structure(xtrain,ytrain)
    #cv = LeaveOneOut(dstr.n_instances,indices=True)
    cv = LeaveOneOut(dstr.n_instances,indices=True)
    best_DG = nx.empty_graph(0,create_using=nx.DiGraph())
    best_accuracy = 0
    best_unused_nodes = set(range(dstr.n_vars))
    changed = True
    while changed:
        changed = False
        DG = best_DG.copy()
        unused_nodes = copy(best_unused_nodes)
        used_nodes = DG.nodes()
        for node_i in unused_nodes:
            new_DG = DG.copy()
            new_unused_nodes = copy(unused_nodes)
            new_unused_nodes.remove(node_i)
            new_DG.add_node(node_i)
            new_accuracy = evaluate_network(dstr, new_DG, xtrain, ytrain, cv)
            if new_accuracy > best_accuracy:
                best_accuracy = new_accuracy
                best_DG = new_DG.copy()
                best_unused_nodes = copy(new_unused_nodes)
                changed = True
            for node_j in used_nodes:
                new_DG = DG.copy()
                if node_i < node_j:
                    new_DG.add_edge(node_i,node_j)
                else:
                    new_DG.add_edge(node_j,node_i)
                new_accuracy = evaluate_network(dstr, new_DG, xtrain, ytrain, cv)
                if new_accuracy > best_accuracy:
                    best_accuracy = new_accuracy
                    best_DG = new_DG.copy()
                    best_unused_nodes = copy(new_unused_nodes)
                    changed = True
    return best_DG