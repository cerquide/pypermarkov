from __future__ import division
from sklearn.cross_validation import LeaveOneOut, KFold
import numpy as np;
import networkx as nx
import math
import itertools

import graph_tools
import dataset;
import gaussian_clique_tree;
import mixture_model;
import inducers;
import measures;
import structures
import Dirichlet

from copy import copy

def naive_bayes_ct_learner(dstr,xtrain,ytrain,options):
    """
    Builds the structure of a junction tree for a Naive Bayes classifier
    :param dstr: information of the structure of the dataset.
    :type dstr: dataset.data_structure.
    :param xtrain: array with the features of the train instances.
    :type xtrain: np.array
    :param ytrain: array with the class of the train instances
    :type ytrain: np.array
    :param options: options of the method.
    :type options: dictionary
    """
    clique_domains = [None] * dstr.n_vars
    separator_domains = [];
    for var_i in xrange(dstr.n_vars):
        clique_domains[var_i] = [var_i];
    return (clique_domains,separator_domains)

def k_jan_ct_learner(dstr,xtrain,ytrain,options):
    """
    Builds the structure of a junction tree for a k-jan classifier as explain in 
    Cite required
    :param dstr: information of the structure of the dataset.
    :type dstr: dataset.data_structure.
    :param xtrain: array with the features of the train instances.
    :type xtrain: np.array
    :param ytrain: array with the class of the train instances
    :type ytrain: np.array
    :param options: options of the method. 'group_size' field is required, the value of this field is the value for k.
    :type options: dictionary
    """
    group_size = options['group_size'];
    n_groups = int(math.ceil(dstr.n_vars/group_size));
    clique_domains = [None] * n_groups
    for group_i in xrange(n_groups):
        start = group_i*group_size
        clique_domains[group_i] = range(start,min(dstr.n_vars,start+group_size))
    return (clique_domains,[])

def k_band_ct_learner(dstr,xtrain,ytrain,options):
    """
    Builds the structure of a junction tree for a k-band classifier as explain in 
    Cite required
    :param dstr: information of the structure of the dataset.
    :type dstr: dataset.data_structure.
    :param xtrain: array with the features of the train instances.
    :type xtrain: np.array
    :param ytrain: array with the class of the train instances
    :type ytrain: np.array
    :param options: options of the method. 'group_size' field is required, the value of this field is the value for k.
    :type options: dictionary
    """
    group_size = options['group_size'];
    n_cliques = dstr.n_vars - group_size + 1;
    clique_domains = [None] * n_cliques;
    separator_domains = [None] * (n_cliques - 1);
    #print 'n_groups=',n_groups
    #parents = [None] * dstr.n_vars
    for var_i in range(n_cliques-1):
        end_clique = var_i + group_size
        clique_domains[var_i] = range(var_i,var_i + group_size);
        separator_domains[var_i] = range(var_i+1,var_i + group_size);
    clique_domains[n_cliques-1] = range(n_cliques-1,n_cliques-1 + group_size);
    return (clique_domains,separator_domains)

def wcGJAN_ct_learner(dstr,xtrain,ytrain,options):
    """
    Builds the structure of a junction tree for wcGJAN classifier as explain in
    Perez, A. (2010). Supervised classification in continuous domains with Bayesian networks. Universidad del Pais Vasco. 
    Cite required
    :param dstr: information of the structure of the dataset.
    :type dstr: dataset.data_structure.
    :param xtrain: array with the features of the train instances.
    :type xtrain: np.array
    :param ytrain: array with the class of the train instances
    :type ytrain: np.array
    :param options: options of the method. 'group_size' field is required, the value of this field is the value for k.
    :type options: dictionary
    """
    G = structures.wcGJAN_learner(dstr, xtrain, ytrain, options, evaluate_network)
    return extract_cliques_and_separators(G)

def wbGJAN_ct_learner(dstr,xtrain,ytrain,options):
    """
    Builds the structure of a junction tree for wbGJAN classifier as explain in
    Perez, A. (2010). Supervised classification in continuous domains with Bayesian networks. Universidad del Pais Vasco. 
    Cite required
    :param dstr: information of the structure of the dataset.
    :type dstr: dataset.data_structure.
    :param xtrain: array with the features of the train instances.
    :type xtrain: np.array
    :param ytrain: array with the class of the train instances
    :type ytrain: np.array
    :param options: options of the method. 'group_size' field is required, the value of this field is the value for k.
    :type options: dictionary
    """
    G = structures.wbGJAN_learner(dstr, xtrain, ytrain, options, evaluate_network)
    return extract_cliques_and_separators(G)       

def wfGJAN_ct_learner(dstr,xtrain,ytrain,options):
    """
    Builds the structure of a junction tree for wcGJAN classifier as explain in
    Perez, A. (2010). Supervised classification in continuous domains with Bayesian networks. Universidad del Pais Vasco. 
    Cite required
    :param dstr: information of the structure of the dataset.
    :type dstr: dataset.data_structure.
    :param xtrain: array with the features of the train instances.
    :type xtrain: np.array
    :param ytrain: array with the class of the train instances
    :type ytrain: np.array
    :param options: options of the method. 'group_size' field is required, the value of this field is the value for k.
    :type options: dictionary
    """
    G = structures.wfGJAN_learner(dstr, xtrain, ytrain, options, evaluate_network)
    return extract_cliques_and_separators(G)
 
def evaluate_network(dstr, DG, x, y,  cv):
    """
    Evaluates a networks in terms of its accuracy performing LeaveOneOut with a set
    :param dstr: information of the structure of the dataset.
    :type dstr: dataset.data_structure
    :param DG: Structure of the network
    :type DG: nx.DirectedGraph
    :param x: classes of the instances
    :type x: np.array
    :param y: classes of the instances.
    :type y: np.array
    """
    cliques,separators = extract_cliques_and_separators(DG)
    options = {};
    inducer = bma_gct_inducer(options)
    model = inducer.learn_parameters(dstr,x,y,cliques,separators,options)
    acc = np.zeros((len(cv),1))
    i = 0
    for (train_idxs,test_idxs) in cv:
        loo_model = model.get_loo_model(y[test_idxs],x[test_idxs,:])
        lp = loo_model.predict(x[test_idxs])
        acc[i] = measures.accuracy(dstr, lp, y[test_idxs])
        i += 1   
    return np.mean(acc)

def extract_cliques_and_separators(DG):
    """
    Create a junction tree and extract the cliques and the separators of the graph.
    \cite required
    :param DG: Structure of the network.
    :type DG: Directed Graph
    """
    moralized_G = graph_tools.moralize(DG) 
    juntion_G = graph_tools.triangulate(moralized_G)
    setlist = nx.chordal_graph_cliques(juntion_G)
    cliquelist = list(setlist)
    juntion_T = graph_tools.juntion_tree(cliquelist)
    cliques_domain = [None] * len(setlist)
    i = 0
    for clique in setlist:
        cliques_domain[i] = list(clique)
        i += 1
    separator_domain = []
    i = 0
    for edge in juntion_T.edges():
        clique_0 = cliquelist[edge[0]]
        clique_1 = cliquelist[edge[1]]
        separator = clique_0.intersection(clique_1)
        separator_domain.append(list(separator))
    separator_domain = list(separator_domain)
    return (cliques_domain, separator_domain)



ct_learners = {};
ct_learners['naive_bayes'] = naive_bayes_ct_learner
ct_learners['k_jan'] = k_jan_ct_learner
ct_learners['k_band'] = k_band_ct_learner
ct_learners['wcGJAN'] = wcGJAN_ct_learner
ct_learners['wbGJAN'] = wbGJAN_ct_learner
ct_learners['wfGJAN'] = wfGJAN_ct_learner

class bma_gct_inducer(inducers.inducer):
    def __init__(self,options):
        self.options = options;
        if (not self.has_learner()):
            self.options['ct_learner'] = 'naive_bayes';

    def has_learner(self):
        return self.options.has_key('ct_learner');

    def learn(self,dstr,xtrain,ytrain):
        (clique_domains,separator_domains) = ct_learners[self.options['ct_learner']](dstr,xtrain,ytrain,self.options);

        model = self.learn_parameters(dstr,xtrain,ytrain,clique_domains,separator_domains,self.options)
        return model;

    def learn_parameters(self,dstr,xtrain,ytrain,clique_domains,separator_domains,options):

        #print xtrain.shape
        # Assess the unconditional probabilities for each class.
        
        dirichlet = Dirichlet.laplacePrior(dstr)
        dirichlet.incorporate_evidence(ytrain)
        ppd = dirichlet.get_posterior_predictive_distribution()
        # Divide the training set in classes
        inst_by_class = dataset.split_by_classes(dstr,xtrain,ytrain)

        # Assess the parameters for each of the classes
        gcts = [None] * dstr.n_classes
        model_ts = [None] * dstr.n_classes

        for class_i in range(dstr.n_classes):
            #print inst_by_class[class_i].shape;
            #print inst_by_class[class_i];
            gcts[class_i] = gaussian_clique_tree.prior(dstr.n_vars, clique_domains, separator_domains)
            gcts[class_i].incorporate_evidence(inst_by_class[class_i])
            model_ts[class_i] = gcts[class_i].get_posterior_predictive_distribution()
        return mixture_model.mixture_model_loo(dstr,dirichlet,ppd,gcts,model_ts)
        
class bma_k_jan_inducer(bma_gct_inducer):
    def __init__(self,group_size):
        options = {}
        options['group_size'] =  group_size
        options['ct_learner'] = 'k_jan'
        bma_gct_inducer.__init__(self,options)
        self.name = 'BMA_K_JAN_'+str(group_size)
        #self.group_size = group_size;

class bma_k_band_inducer(bma_gct_inducer):
    def __init__(self,group_size):
        options = {}
        options['group_size'] =  group_size
        options['ct_learner'] = 'k_band'
        bma_gct_inducer.__init__(self,options)
        self.name = 'BMA_K_BAND_'+str(group_size)
        #self.group_size = group_size;
        
class bma_wcGJAN_inducer(bma_gct_inducer):
    def __init__(self):
        options = {}
        options['ct_learner'] = 'wcGJAN'
        bma_gct_inducer.__init__(self,options)
        self.name = 'BMA_wcGJAN'

class bma_wbGJAN_inducer(bma_gct_inducer):
    def __init__(self):
        options = {}
        options['ct_learner'] = 'wbGJAN'
        bma_gct_inducer.__init__(self,options)
        self.name = 'BMA_wbGJAN'

class bma_wfGJAN_inducer(bma_gct_inducer):
    def __init__(self):
        options = {}
        options['ct_learner'] = 'wfGJAN'
        bma_gct_inducer.__init__(self,options)
        self.name = 'BMA_wfGJAN'

