import numpy as np;
import math;
import numpy.random as nr;

import numpy.linalg as linalg;
import maths;
from distributions import *;

class mvn(distribution):
    def __init__(self,mu,Sigma):
        self.dim = mu.shape[0];
        self.mu = mu;
        self.Sigma = Sigma;
        self.sharp = (np.amax(Sigma) == 0.0);
        self.log_constant = self._assess_log_constant();

    def _assess_log_constant(self):
        ctte = -0.5 * self.dim * math.log(2. * math.pi);
        (s,l) =  linalg.slogdet(self.Sigma);
        return ctte - 0.5 * l; 

    def logpdf(self,x):
        d = x-self.mu;
        if self.sharp:
            return np.zeros(x.shape[0]) - float('inf') * np.absolute(np.sum(d*d,axis=1)) 
        else:
            return -0.5 * maths.quadratic_form_of_inverse(self.Sigma,d) + self.log_constant

    
    def sample(self,n):
        return nr.multivariate_normal(self.mu,self.Sigma,n);

def learn_ml(x):
        (n_instances,n_vars) = x.shape;
        mu = np.mean(x,axis=0);
        #if (n_vars > n_instances):
        #    sigma = np.eye(n_vars);
        #else:
        sigma = maths.cov_always_matrix(x); #np.cov(x,rowvar=0);
        #if sigma.ndim==0:
        #    sigma.shape=(1,1);
        #print 'ML mu=',mu
        #print 'ML sigma=',sigma
        #print linalg.eig(sigma);
        return mvn(mu,sigma);

def logpdf(x,mu,Sigma):
    dim_ = mu.shape[0];
    ctte = -0.5 * dim_ * math.log(2. * math.pi);
    (s,l) =  linalg.slogdet(Sigma);
    log_constant = ctte - 0.5 * l; 
    return -0.5 * maths.quadratic_form_of_inverse(Sigma,x - mu) + log_constant;

if __name__ == '__main__':
    from mvn import *
    m = mvn(np.array([1,4]),np.array([[2,1],[1,3]]));
    sol = np.array([-5.34259602 -2.94259602]);
    sol2 = m.logpdf(np.array([[2,1],[2,4]]));
    assert (sum(sol-sol2)<1e-9);
    

