'''
Created on 12/07/2012

@author: victor
'''
from matplotlib import cm
from sys import argv, exit
from tables import file
import calibration
import dataset
import experiments
import matplotlib.pyplot as plt
import numpy as np


def num_vars(num_classes,num_vars,classifier_type,group_sizes):
    if 'BAND' in classifier_type:
        return num_classes*num_vars*(group_sizes+(group_sizes*(group_sizes+1))/2)/group_sizes
    else:
        return num_classes*(group_sizes+1)*(num_vars-(group_sizes*(group_sizes-1))/2)     

def get_tasks_results(fileh, tasks,v):
    results = []
    measures = ['accuracy','cll','learning_time','prediction_time']
    for task in tasks:
        name = task._v_name
        if v:
            print 'Task', name
        t = experiments.load_task(task._v_attrs)
        m = t.get_results(fileh,measures)
        n_vars = task._f_getAttr('n_vars')
        n_classes = task._f_getAttr('n_classes') 
        results.append(task_results(name, m, measures, n_vars, n_classes))
        
    return results    
        
    
class task_results:
    def __init__(self, name,results, measures, n_vars, n_classes):
        self.name = name
        self.results = results
        self.k_methods = self.asses_k_methods(results.keys())
        self.methods = self.asses_methods(results.keys())
        self.measures = measures
        self.n_vars = n_vars
        self.n_classes = n_classes
        
    def asses_methods(self,methods):
        selected = []
        for method in methods:
            if not '_K_' in method:
                selected.append(method)
        return selected
    
    def asses_k_methods(self,methods):
        selected = []
        for method in methods:
            if '_K_' in method:
                selected.append(method)
        return selected
    def plot(self):       
        for m_i,measure in enumerate(self.measures):
            if len(self.methods):
                self.plot_method(m_i, measure)       
            if len (self.k_methods):
                self.plot_k_method(m_i, measure)
        
    def plot_k_method(self,m_i, measure):
        plt.figure(figsize=(11,7))
        x, y = self.organize_k_methods(m_i)
        for m_j, method in enumerate(x.keys()):
            plt.plot(x[method],y[method], label = method, color = cm.jet(1.*m_j/len(x.keys())))
        
        plt.legend(loc='upper right')
        plt.ylabel(measure)
        plt.xlabel('Model')
        plt.title(self.name)
        plt.show()
    def plot_method(self,m_i, measure):
        plt.figure(figsize=(11,7))
        d = dataset.load('../data/vehicle')
        
        for m_j,method in enumerate(self.methods):               
            m = self.results[method][m_i,:,:]
            cal = calibration.potential_calibration(m,d.y,d.str)
            m_cal = cal.calibrate(m)
            m2 = np.mean(m_cal,axis=1)
            m3 = np.mean(m2,axis=0)        
            plt.bar(m_j,m3,width=1,align='center',color=cm.jet(1.*m_j/len(self.methods)))
        plt.title(self.name)
        plt.xticks(range(len(self.methods)), self.methods)
        plt.ylabel(measure)
        plt.xlabel('Model')
        plt.savefig(self.name+'-'+measure+'.pdf');
        plt.show() 
        
    def organize_k_methods(self,m_i):
        methods_max = {}
        for method in self.k_methods:
            digits = len(method.split('_')[-1])
            name = method[0:-digits] 
            if not name in methods_max:
                methods_max[name] = int(method[-digits:])
            else:
                k = int(method[-digits:])
                if k > methods_max[name]:
                    methods_max[name] = k
        x = {}
        y = {}
        for name in methods_max.iterkeys():
            x[name] = np.array([0]*methods_max[name])
            y[name] = np.array([0]*methods_max[name])
        for method in self.k_methods:
            digits = len(method.split('_')[-1])
            name = method[0:-digits] 
            k = int(method[-digits:])
            
            num_classes = self.n_classes
            num_vars = self.n_vars
            
            x[name][k-1] = num_vars(num_classes,num_vars,method,k)
            m = self.results[method][m_i,:,:]
            m2 = np.mean(m,axis=1)
            m3 = np.mean(m2,axis=0)  
            y[name][k-1] = m3
        for name in x.keys():
            y[name] = y[name][x[name]!= 0]
            x[name] = x[name][x[name]!= 0]
        return x, y 

if __name__ == '__main__':
    v = False
    if len(argv) == 1:
        print 'File name is required'
        exit(0)
    if '-v' in argv:
        v = True
    
    file_name = argv[1]
    file_results = file.openFile(file_name,'r')
    if v:
        print 'File:', file_name
    root = file_results.getNode('/')
    tasks = root._f_iterNodes()
    results = get_tasks_results(file_results, tasks, v)
    for result in results:
        result.plot()
        
    try: 
        file_results.close()
    except Exception as e:
        print 'Closing file'
    