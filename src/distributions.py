# Base class for probability distributions

import numpy as np;

class distribution:
    def logpdf(self,x):
        return np.zeros(x.shape[0]);


class factor(distribution):
    def __init__(self,dim,domain,distribution):
        self.dim = dim
        self.domain = domain
        self.distribution = distribution

    def logpdf(self,x):
        return self.distribution.logpdf(x[:,self.domain])

    def jointDimension(self):
        return self.dim

    def __str__(self):
        return "[Factor: dim = "+str(self.dim)+" domain = "+str(self.domain) + " distribution = "+str(self.distribution)+"]\n"
    


class factored_distribution(distribution):
    def __init__(self,cliques,separators):
        self.dim = cliques[0].jointDimension();
        self.cliques = cliques;
        self.separators = separators;
    
    def logpdf(self,x):
        n = x.shape[0];
        logP = np.zeros(n);
        #print "x=",x
        #print logP
        for clique in self.cliques:
            #print clique
            #print clique.logpdf(x);
            logP = logP + clique.logpdf(x);
        for separator in self.separators:
            logP = logP - separator.logpdf(x);
        return logP;

    def __str__(self):
        return "[FactoredDistribution: dim = "+str(self.dim)+" cliques = ".join(map(str,self.cliques)) + " separators = ".join(map(str,self.separators))+"]\n"
    
