import experiments
import matplotlib.pyplot as plt
import re
import numpy as np
  

labels={}
labels['ML-wcGJAN']= 'ML-wcGJAN';
labels['BMA-wcGJAN']= 'BMA-wcGJAN';
measureLabels={}
measureLabels['cll']= 'Conditional log likelihood';
measureLabels['accuracy']= 'Accuracy';
measureLabels['learning_time']= 'Learning time';
measureLabels['prediction_time']= 'Prediction time';



def plot_comparison(task,classifier_types,measures,results):
    for (m_i,measure) in enumerate(measures):
        f = plt.figure();
        #f_errorbar = figure();
        colors=['#ff8c00','#6495ed','#ff8c00','#6495ed','#ff8c00','#6495ed'];
        i = 0
        for classifier_type in classifier_types:
            m = get_measure(m_i,classifier_type,results);
            m2 = np.average(m,axis=1);
            print m2
            m3 = np.average(m2,axis=0);
            print m3
            plt.bar(i,m3,width=1,align='center',color=colors[i])
            i = i+1
        plt.xticks(range(i),classifier_types)
        plt.ylabel(measureLabels[measure])
        plt.xlabel('Model')
        
        plt.legend(loc='upper right')
        plt.savefig(task.name+'-'+measure+'-nv.pdf');
        plt.show() 

def get_measure(m_i,classifier_type,results):
    n_measures,n_folds,n_runs = results[results.keys()[0]].shape
    m = np.zeros([n_folds,n_runs])
    m[:,:] = results[classifier_type][m_i,:,:]
    return m
#        avg_measure_run = reshape(measure,numel(group_sizes),runs*folds);
#        avg_measure = mean(avg_measure_run,2);
#        prcs = prctile(avg_measure_run,[40,50,60],2);
#        median = prcs(:,2)';
#        lower = prcs(:,2)' - prcs(:,1)';
#        upper = prcs(:,3)' - prcs(:,2)';
#        figure(f);
#        plot(group_sizes,median,colors{i});
#        hold;
#        figure(f_errorbar);
#        errorbar(group_sizes,median,lower,upper,colors{i});
#        hold;
#        i = i + 1;
#    end
#    figure(f);
#    do_legend(classifier_types,measure_name);
#    saveas(f,['fig/' file_name '.' measure_name '.median.plot.pdf']);
#    close(f);
#    figure(f_errorbar);
#    do_legend(classifier_types,measure_name);
#    saveas(f_errorbar,['fig/' file_name '.' measure_name '.errorbar.plot.pdf']);
#    close(f_errorbar);
#end

#function do_legend(classifier_types,measure_name)
#    legend(classifier_types);
#    xlabel('Size of the groups');
#    ylabel(measure_name);
#end

def determine_group_sizes(results,classifier_name):
    group_sizes = [];
    classifiers = results.keys()
    for classifier_i_name in classifiers:
        n = get_group_size(classifier_i_name,classifier_name);    
        if n > 0:
            group_sizes.append(n);
    group_sizes.sort()
    return group_sizes

def get_group_size(classifier_i_name,classifier_name):
    prog = re.compile(classifier_name + '-(\d*)');
    m = prog.match(classifier_i_name);
    if m:
        print m.group(1)
        return int(m.group(1))
    else:
        return 0
  
#function  [real_runs] = determine_real_runs(results,runs,dataset_i)
#    real_runs = runs;
#    for run = 1 : runs
#        if (numel(results.fold_results{dataset_i}{run,1}) == 0)
#            real_runs = run - 1;
#            return
#        end
#    end
#end

#function m = get_measure(measure_name,classifier_name,results,group_sizes,runs,folds,dataset_i)
#    n_groups = numel(group_sizes);
#    m = zeros(n_groups,runs,folds);
#    for run = 1 : runs
#        for fold = 1: folds
#            rf_results = results.fold_results{dataset_i}{run,fold};
#            group_i = 1;
#            for classifier_i = 1 : numel(rf_results)
#                n = get_group_size(rf_results{classifier_i}.name,classifier_name);
#                if n > 0
#                    f = getfield(rf_results{classifier_i}.metrics,measure_name);
#                    m(group_i,run,fold) = f;
#                    group_i = group_i + 1; 
#                else
#                    'No';
#                end
#            end
#        end
#    end
#end

h = experiments.experiment_handle('test1','test1.hdf5');
e = experiments.experiment.open(h,'r');
#t = e.get_task('1CV10.1');
#t.runs = 5;
t = e.get_task('vehicle');
measures = ['accuracy','cll','learning_time','prediction_time'];
m = t.get_results(e.fileh,measures);
inducerNames =  ['ML_wcGJAN','BMA_wcGJAN','ML_wbGJAN','BMA_wbGJAN','ML_wfGJAN','BMA_wfGJAN'];
plot_comparison(t,inducerNames,measures,m);

e.close();
        
       
