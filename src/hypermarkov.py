# Base class for probability distributions

import distributions
import mixture_model
import copy 

class conjugate_distribution(distributions.distribution):
    def incorporate_evidence(self,x):
        pass
    
    def remove_evidence(self,x):
        pass
    
    def get_loo_distribution(self,x):
        loo_model = copy.deepcopy(self)
        loo_model.remove_evidence(x)
        return loo_model
   
    def get_posterior_predictive_distribution(self):
        pass

    def assess_posterior(self,x):
        posterior = copy.deepcopy(self)
        posterior.incorporate_evidence(x);

class conjugate_factor(distributions.factor,conjugate_distribution):
    def __init__(self,dim,domain,cd):
        #super(conjugate_factor,self).__init__(dim,domain,cd);
        distributions.factor.__init__(self,dim,domain,cd);
    
    def incorporate_evidence(self,x):
        self.distribution.incorporate_evidence(x[:,self.domain]);
        
    def remove_evidence(self,x):
        self.distribution.remove_evidence(x[:,self.domain]);

    def get_posterior_predictive_distribution(self):
        # We should check here whether ppd is conjugate and return a conjugate_factor in that case.
        ppd = self.distribution.get_posterior_predictive_distribution();
        return distributions.factor(self.dim,self.domain,ppd);

class strong_hypermarkov_distribution(distributions.factored_distribution,conjugate_distribution,mixture_model.loo_mixture_component):
    def __init__(self,cliques,separators):
        distributions.factored_distribution.__init__(self,cliques,separators)

    def incorporate_evidence(self,x):
        self._incorporate_evidence_in_factors(self.cliques,x)
        self._incorporate_evidence_in_factors(self.separators,x)
        
    def remove_evidence(self,x):
        self._remove_evidence_from_factors(self.cliques,x)
        self._remove_evidence_from_factors(self.separators,x)

    def get_posterior_predictive_distribution(self):
        cliques_ppd = self._get_ppd_factors(self.cliques)
        separators_ppd = self._get_ppd_factors(self.separators)
        return distributions.factored_distribution(cliques_ppd,separators_ppd)
        
    def get_loo_model(self,x):
        loo_distrib = self.get_loo_distribution(x)
        return loo_distrib.get_posterior_predictive_distribution()
    
    def _incorporate_evidence_in_factors(self,factors,x):
        for factor in factors:
            factor.incorporate_evidence(x)
    
    def _remove_evidence_from_factors(self,factors,x):
        for factor in factors:
            factor.remove_evidence(x)

    def _get_ppd_factors(self,factors):
        return [factor.get_posterior_predictive_distribution() for factor in factors]
        #n_factors = len(factors);
        #if (n_factors == 0):
        #    return [];
        #else:
        #    ppd_factors = [None]*n_factors;
        #    for factor_i in xrange(n_factors):
        #        ppd_factors(factor_i) = 
                