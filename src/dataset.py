import scipy.io;
import numpy as np;

class dataset_handle:
    """Class corresponding to dataset_handle"""
    def __init__(self,name,filename):
        """Initialize a daset handle
        @param name: Name of the dataset
        @param filename: Name of the file
        """
        self.name = name;
        self.filename = filename;

    def load(self):
        """Load a dataset handle by the daset_handle object."""
        return load(self.filename);

class dataset_structure:
    """Class where is keeped information of datasets.""" 
    def __init__(self,x,y):
        """Initialization of the dataset_structure object"""
        (self.n_instances,self.n_vars) = x.shape;
        self.class_values = np.unique(y);
        self.n_classes = self.class_values.size;
        self.class_index={};
        #enumerate enumerate the different class values. class_index is a dictionary.
        for (i,x) in enumerate(self.class_values):
            self.class_index[x]=i;

    def __str__(self):
        x  = '[n_instances ='+ str(self.n_instances) + ",";
        x  = x + 'n_vars ='+ str(self.n_vars) + ",";
        x  = x + 'n_classes ='+ str(self.n_classes) + ",";      
        x  = x + 'class_values ='+ str(self.class_values) + ","; 
        x  = x + 'class_index ='+ str(self.class_index) + "]";
        return x;
    
class dataset:
    """Class containing dataset information."""""
    def __init__(self,x,y):
        """"Initialize dataset object
        @param x: matrix corresponding to the samples of data in the row and features in the columns.
        @param y: Classes of the corresponding x.
        """
        self.x = x;
        self.y = y;
        self.str = dataset_structure(x,y);

    def __str__(self):    
        x  = '[str ='+str(self.str)+ ",";
        x  = x + 'x ='+str(self.x)+ ",";
        x  = x + 'y ='+str(self.y)+ "]";
        return x;

def load(filename):
    """ Loads a dataset contained in a .mat file that includes x data and y data."""
    d = scipy.io.loadmat(filename,mat_dtype=True);
    return dataset(d['x'],d['y']);
    #print d;

def split_by_classes(dstr,xtrain,ytrain):
    # Split instances by classes
    #print xtrain.shape;
    #print ytrain.shape;
    inst_by_class=[None]*dstr.n_classes;
    for class_i in range(dstr.n_classes):
        class_cut = xtrain[ytrain.flatten() == dstr.class_values[class_i],:];
        #print "Class cut:",class_cut.shape;
        inst_by_class[class_i] = class_cut;
    return inst_by_class;

if __name__ == '__main__':
    d = load('../data/ovarian_cancer.mat');
    print d;
    bc = split_by_classes(d.str,d.x,d.y);
    print bc[0].shape;
    print bc[1].shape;
    #print d;
